# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Starting client server 

In the client directory, you run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.

## Starting websocket server 

In the server directory, you run:

### `npm start`

## Dependencies 

To install all dependencies, run :

### `npm i`

in both client and server directories