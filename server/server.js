import {initializeApp} from "firebase/app";
import {getDatabase, ref} from "firebase/database";
import {createRequire} from 'module';
import {firebaseConfig} from "./constants.js";
import broadcastMessage from "./utils/broadcastMessage.js";
import {wipeDatabase, setDatabase, getData} from "./utils/database_utils.js"

const require = createRequire(import.meta.url);
const app = initializeApp(firebaseConfig);
const database = getDatabase(app);
const dbRef = ref(database);
const ws = require('ws');
const wss = new ws.Server({
    port: 5000,
}, () => console.log(`Server started on 5000`))

let users = [];

wss.on('connection', (ws) => {
    ws.on('message', (message) => {
        message = JSON.parse(message)
        switch (message.event) {
            case 'message':
                let index = 0;
                while (message.id !== users[index].id) index++;
                message.imageURL = users[index].imageURL;
                setDatabase(message, database);
                broadcastMessage(message, wss, users);
                break;
            case 'connect':
                users.splice(wss.clients.size - 1, 0, {
                    id: message.id,
                    imageURL: message.imageURL,
                });
                broadcastMessage(message, wss, users);
                setDatabase(message, database);
                const client = Array.from(wss.clients)[wss.clients.size - 1];
                getData(client, dbRef);
                break;
        }
    })

    ws.on('close', () => {
        if (wss.clients.size === 0)
            wipeDatabase(dbRef, database);
    })
})