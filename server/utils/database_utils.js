import {child, get, ref, remove, set} from "firebase/database";

const wipeDatabase = (dbRef, database) => {
    get(child(dbRef, 'messages/')).then((snapshot) => {
        const data = snapshot.val();
        for (let message in data) {
            const messageRef = ref(database, 'messages/' + message);
            remove(messageRef);
        }
    });
}

const setDatabase = (message, database) => {
    set(ref(database, 'messages/message-' + message.id + '-' + message.time), message);
}

const getData = (client, dbRef) => {
    get(child(dbRef, 'messages/')).then((snapshot) => {
        const snapshot_value = snapshot.val();
        let data = [];

        for (let message in snapshot_value) {
            data.push(snapshot_value[message]);
        }

        function compare(a, b) {
            if (a.time < b.time) {
                return -1;
            }
            if (a.time > b.time) {
                return 1;
            }
            return 0;

        }

        data.sort(compare);
        data.forEach(message => {
            client.send(JSON.stringify(message));
        })
    });
};

export {wipeDatabase, setDatabase, getData};