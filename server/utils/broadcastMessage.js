const broadcastMessage = (message, wss, users) => {
    Array.from(wss.clients).forEach(function (client, index) {
        try {
            message.isCurrentUser = users[index].id === message.id;
            if (message.isCurrentUser) {
                message.imageURL = 'https://randomuser.me/api/portraits/lego/1.jpg';
            } else {
                for (let user of users) {
                    if (user.id === message.id) {
                        message.imageURL = user.imageURL;
                        break;
                    }
                }
            }
            client.send(JSON.stringify(message))
        } catch (e) {
            console.log(e)
        }
    })
}

export default broadcastMessage;