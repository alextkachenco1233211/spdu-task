const firebaseConfig ={
    apiKey: `${process.env.REACT_APP_FIREBASE_API_KEY}`,
    authDomain: "test-react-database-b2db5.firebaseapp.com",
    projectId: "test-react-database-b2db5",
    storageBucket: "test-react-database-b2db5.appspot.com",
    messagingSenderId: "449434663565",
    appId: "1:449434663565:web:110207b4de99f5f1a100c4",
    databaseURL: "https://test-react-database-b2db5-default-rtdb.firebaseio.com",
}

export {firebaseConfig};
