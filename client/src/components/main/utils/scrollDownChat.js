const scrollDownChat = (chatRef) => {
    chatRef.current.scrollTo(0, chatRef.current.scrollHeight);
}

export {scrollDownChat};