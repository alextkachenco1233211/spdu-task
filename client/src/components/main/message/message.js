import React from 'react';
import './message.css'

const Message = ({text, isCurrentUser, imageURL}) => {
    return (
        <div className={`message ${isCurrentUser ? '' : 'message_second_user'}`}>
            <img src={imageURL} alt="user" className="message__avatar"/>
            <p className="message__text">{text}</p>
        </div>
    );
};

export default Message;
