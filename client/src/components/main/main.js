import React, {useEffect, useRef, useState} from 'react';
import Message from "./message/message";
import LoadingScreen from "./loading_screen/loading_screen";
import './main.css'
import {scrollDownChat} from "./utils/scrollDownChat";


const Main = () => {
    const [messages, setMessages] = useState([]);
    const [newMessage, setNewMessage] = useState('');
    const [userId, setUserId] = useState(null);
    const [isConnected, setIsConnected] = useState(false);
    const messages_wrapper = useRef(null);
    const socket = useRef();

    useEffect(() => {
        const userId = Date.now();

        setUserId(userId);

        socket.current = new WebSocket('ws://localhost:5000');

        socket.current.onopen = () => {
            const xhr = new XMLHttpRequest();
            xhr.open('GET', 'https://picsum.photos/320/200?random=1');
            xhr.send();
            xhr.onload = async function () {
                const message = {
                    id: userId,
                    event: 'connect',
                    imageURL: xhr.responseURL,
                    time: Date.now(),
                }
                setIsConnected(true);
                socket.current.send(JSON.stringify(message));
            }
        }

        socket.current.onmessage = (input) => {
            const input_data = JSON.parse(input.data)
            let imageURL, value;
            const isCurrentUser = input_data.id === userId;
            if (input_data.event === 'connect') {
                if (isCurrentUser) {
                    return;
                }
                value = 'New user connected';
                imageURL = 'https://randomuser.me/api/portraits/lego/2.jpg';
            } else {
                value = input_data.message;
                imageURL = input_data.imageURL;
            }
            setMessages(prevState => [...prevState, {text: value, isCurrentUser: isCurrentUser, imageURL: imageURL}]);
            setNewMessage('');
        }

        socket.current.onerror = (e) => {
            console.log(`Error: ${e}`)
        }
    }, [])


    const messageSendHandler = async () => {
        if (newMessage) {
            const message = {
                message: newMessage,
                id: userId,
                event: 'message',
                time: Date.now(),
            }
            socket.current.send(JSON.stringify(message));
        }
    }

    const messageTypingHandler = ({target: {value}}) => {
        if (value.trim()) {
            setNewMessage(value);
        }
    }

    useEffect(() => {
        scrollDownChat(messages_wrapper);
    }, [messages]);

    return (
        <main className="main">
            {isConnected || <LoadingScreen/>}
            <section className="main__messages_wrapper" ref={messages_wrapper}>
                {messages.map((message, i) => <Message {...message} key={i}/>)}
            </section>
            <section className={"main__section"}>
                <textarea className={"main__textarea"} onChange={messageTypingHandler} value={newMessage}/>
            </section>
            <section className={"main__section"}>
                <button className={"main__button"} onClick={messageSendHandler}>Send message</button>
            </section>
        </main>
    );
};

export default Main;