import React from 'react';
import './loading_screen.css'

const LoadingScreen = () => {
    return (
        <div className="main__loading_screen">
            <i className="fa-solid fa-wifi loading_screen__icon"></i>
            <h1 className="loading_screen__text">Connecting to server...</h1>
        </div>
    );
};

export default LoadingScreen;